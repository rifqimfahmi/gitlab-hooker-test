## How to run

```
cd docker/
docker-compose up --build
```

## How to stop the service

```
cd docker/
docker-compose down
```