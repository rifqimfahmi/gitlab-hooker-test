from flask import json, request, Flask

app = Flask(__name__)

# Local test curl command:
# curl -i -X POST -H 'Content-Type: application/json' -d '{"name": "New item", "year": "2009"}' http://localhost:5000

@app.route('/', methods=['POST'])
def hook_it():
    return f"Hello hookers! {request.json}"
 
if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)